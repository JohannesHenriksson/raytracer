#ifndef MODEL_H_
#define MODEL_H_


#include <vector>
#include <map>
#include <d3d11.h>
#include <DirectXMath.h>
#include <string>

class Model
{
public:
	//Fixa alignment
	struct Vertex
	{
		Vertex(){}
		Vertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float texU, float texV)
			: Position(posX, posY, posZ), Normal(normX, normY, normZ), TexCoords(texU, texV){}

		DirectX::XMFLOAT3 Position;
		DirectX::XMFLOAT3 Normal;
		DirectX::XMFLOAT2 TexCoords;
		int materialId;
		//Alla materials kommer ligga i vector, som vi skapar
		//structured buffer av o skickar till shadern. Varje vertex f�r en 
		//uint som s�ger vilket index i material listan som den anv�nder
	};
	struct Material {
		DirectX::XMFLOAT3 Kd = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f);
		float transparency = 0.0f; //d or Ts in mtllib

		DirectX::XMFLOAT3 SpecularColor = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f); //Specular Color
		float reflectivity = 0.5f;

		int textureIndex = -1;
		float Ni = 0.0f; //Optical Density, aka index of refraction
		float Ns = 1000.0f; //Specular Exponent
		float pad1 = 0;

		DirectX::XMFLOAT4 pad2 = DirectX::XMFLOAT4(0, 0, 0, 0);


		Material(){};
		Material(float red, float green, float blue, float transp, float reflect) :
			Kd(red, green, blue), transparency(transp), reflectivity(reflect){};
		Material(float red, float green, float blue, float specRed, float specGreen, float specBlue, float transp, float reflect) :
			Kd(red, green, blue), SpecularColor(specRed, specGreen, specBlue),
			transparency(transp), reflectivity(reflect){};
	};
	struct Triangle {
		DirectX::XMFLOAT3 trianglePositions[3];
		unsigned int mortonCode = 0;
		DirectX::XMFLOAT3 normal;
		int materialId;
		DirectX::XMFLOAT2 texCoords[3];
	};

	Model();
	~Model();

	bool Load(std::string filePath, int textureOffset, int materialOffset);
	std::vector<Vertex>& GetVertices(){ return vertices_; };
	std::vector<UINT>& GetIndices(){ return indices_; };
	std::vector<Triangle>& GetTriangles() { return triangles_; };
	std::vector<Material>& GetMaterials() { return materials_; };
	std::vector<ID3D11ShaderResourceView*>& GetTextures() { return textures_; };
	std::vector<ID3D11Resource*>& GetTexResources() { return texResources_; };


private:

	void CalculateNormal(Triangle t);
	bool CompareVec(DirectX::XMFLOAT3 v1, DirectX::XMFLOAT3 v2);
	bool CompareVec(DirectX::XMFLOAT2 v1, DirectX::XMFLOAT2 v2);



	//		 MaterialName, Index
	std::map<std::string, UINT> materialMap_;
	//		TextureFileName, Index
	std::map<std::string, UINT> textureMap_;

	//In till shader
	std::vector<Material> materials_;
	std::vector<Vertex> vertices_;
	std::vector<unsigned int> indices_;
	std::vector<Triangle> triangles_;
	std::vector<ID3D11ShaderResourceView*> textures_;
	std::vector<ID3D11Resource*> texResources_;

	int VertexExists(Vertex v);

};

#endif