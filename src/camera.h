
#ifndef CAMERA_H_
#define CAMERA_H_


#include "Input.h"
#include <DirectXMath.h>
#include <d3d11.h>


class Camera
{
public:

	struct CameraBuffer
	{
		DirectX::XMFLOAT4 CamXValues;
		DirectX::XMFLOAT4 CamYValues;
		DirectX::XMFLOAT4 CamZValues;
	};

	Camera();
	~Camera();

	//TODO: Skriv f�rdigt denna
	//Vi kommer nog bara beh�va, position, up, right, och direction. Inga matriser

	void				Update(float dt);


	ID3D11Buffer*		GetConstantBuffer(){ return constantBuffer_;}
	DirectX::XMFLOAT3	GetPosition();
	void				SetPosition(DirectX::XMFLOAT3 position){ position_ = position; }

	void				SetConstantBuffer(unsigned int bindPoint);


private:
	Camera& operator=(const Camera &tmp);
	void UpdateFreeFly(float dt);


	void UpdateRotation();
	void UpdatePosition(float dt);

	void Pitch(float angle);
	void RotateY(float angle);

	void MoveForward(float dt);
	void MoveBackward(float dt);
	void MoveLeft(float dt);
	void MoveRight(float dt);
	void MoveUp(float dt);
	void MoveDown(float dt);

	//////////////////////////////////////////////////////////////////////////
	//Members
	//////////////////////////////////////////////////////////////////////////

	const float						camSpeed = 15.0f;

	ID3D11Buffer*					constantBuffer_;

	DirectX::XMFLOAT3				direction_;
	DirectX::XMFLOAT3				right;
	DirectX::XMFLOAT3				up;
	DirectX::XMFLOAT3				position_;

	float							yaw;
	float							pitch;

	float							angle_;


};

#endif