#ifndef GRAHPICS_H_
#define GRAPHICS_H_

#include <d3d11.h>
#include "window.h"

class Graphics
{
public:
	static Graphics& instance() {
		static Graphics instance_;
		return instance_;
	}
	~Graphics();

	HRESULT Initialize(Window* window);

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();
	ID3D11UnorderedAccessView* GetBackbufferUAV();
	long GetWindowWidth();
	long GetWindowHeight();
	void ClearBackbufferUAV(float r, float g, float b, float a);

	static void Reset();
	void Swap();


private:
	

	Graphics(); 
	Graphics(Graphics const&);
	void operator=(Graphics const&);


	Window*						parentWindow;
	ID3D11Device*				device_ = nullptr;
	ID3D11DeviceContext*		deviceContext_ = nullptr;
	IDXGISwapChain*				swapChain_ = nullptr;

	ID3D11UnorderedAccessView*	backbufferUAV_ = nullptr;
};

#endif //WDGRAPHICS_H_