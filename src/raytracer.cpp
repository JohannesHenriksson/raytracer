#include "raytracer.h"
#include <d3dcompiler.h>
#include <iostream>
#include <DirectXMath.h>
#include "graphics.h"
#include <comdef.h>


Raytracer::Raytracer()
{
}


Raytracer::~Raytracer()
{
	//Shaders
	SafeRelease(primaryRaysCS_);
	SafeRelease(intersectionCS_);
	SafeRelease(colorCS_);
	SafeRelease(classLink);

	//UAVs
	SafeRelease(raysUAV_);
	SafeRelease(colorUAV_);

	//Constant Buffers
	SafeRelease(variableBuffer_);
	SafeRelease(materialBuffer_);
	SafeRelease(lightBuffer_);
	SafeRelease(aabbBuffer_);
	//SRVs
	SafeRelease(trianglesSRV_);
	SafeRelease(spheresSRV_);

	SafeRelease(samplerState_);

	delete model_;
	delete camera_;
	delete timer;
}

bool Raytracer::Init()
{
	timer = new D3D11Timer(Graphics::instance().GetDevice(), Graphics::instance().GetDeviceContext());
	camera_ = new Camera();

	D3D11_SAMPLER_DESC sDesc;
	sDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sDesc.MinLOD = 0;
	sDesc.MaxLOD = FLT_MAX;
	sDesc.MipLODBias = 0.0f;
	sDesc.MaxAnisotropy = 1;
	sDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;

	Graphics::instance().GetDevice()->CreateSamplerState(&sDesc, &samplerState_);

	Graphics::instance().GetDevice()->CreateClassLinkage(&classLink);
	std::string path = "Debug/";

#ifndef _DEBUG
	path = "Release/";
#endif
#ifdef _WIN64
    path = "x64/" + path;
#endif
#pragma region ShaderLoad
	if (!LoadComputeShader(path + "primaryRays.cso", &primaryRaysCS_))
		return false;
	if (!LoadComputeShader(path + "Intersections.cso", &intersectionCS_))
		return false;
	if (!LoadComputeShader(path + "ColorStage.cso", &colorCS_))
		return false;
#pragma endregion
	
	std::vector<Model::Vertex> vertices;


	

	for (int i = 0; i < nrLights; i++)
		lights.push_back(PointLight(8.5f, 10.0f - 2 * i, 1.0f,
			75.0f,
			0.5f, .5f, .5f,
			25.f));


	std::vector<Model::Material> materials;
	materials.push_back(Model::Material(1.0f, 0.0f, 0.0f, 0.0f, 0.1f));//Red Material
	materials.push_back(Model::Material(0.0f, 1.0f, 0.0f, 0.0f, 0.3f));//Green Material
	materials.push_back(Model::Material(0.7f, 0.7f, 0.7f, 0.0f, 0.8f));//Blue Material

	std::vector<Model::Triangle> triangles;
	
#pragma region Room
	//Floor1
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(-25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(-25.0f, -10.0f, -50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	//Floor2
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(25.0f, -10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(-25.0f, -10.0f, -50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	//Walls 1
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(25.0f, 10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(-25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(0.0f, 0.0f, -1.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	//Walls 2
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(-25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(25.0f, 10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(-25.0f, 10.0f, 50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(0.0f, 0.0f, -1.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	//Walls 3
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(25.0f, -10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(25.0f, 10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(-25.0f, -10.0f, -50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	//Walls 4
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(-25.0f, -10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(25.0f, 10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(-25.0f, 10.0f, -50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	//Walls 5
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(-25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(-25.0f, -10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(-25.0f, 10.0f, -50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	triangles[triangles.size() - 1].texCoords[0] = DirectX::XMFLOAT2(0.0f, 1.0f);
	triangles[triangles.size() - 1].texCoords[1] = DirectX::XMFLOAT2(0.0f, 0.0f);
	triangles[triangles.size() - 1].texCoords[2] = DirectX::XMFLOAT2(1.0f, 0.0f);

	//Walls 6
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(-25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(-25.0f, 10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(-25.0f, 10.0f, 50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	triangles[triangles.size() - 1].texCoords[0] = DirectX::XMFLOAT2(0.0f, 1.0f);
	triangles[triangles.size() - 1].texCoords[1] = DirectX::XMFLOAT2(1.0f, 0.0f);
	triangles[triangles.size() - 1].texCoords[2] = DirectX::XMFLOAT2(1.0f, 1.0f);
	//Walls 7
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(25.0f, -10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(25.0f, 10.0f, -50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue

	//Walls 8
	triangles.push_back(Model::Triangle());
	triangles[triangles.size() - 1].trianglePositions[0] = DirectX::XMFLOAT3(25.0f, -10.0f, 50.0f);
	triangles[triangles.size() - 1].trianglePositions[1] = DirectX::XMFLOAT3(25.0f, 10.0f, -50.0f);
	triangles[triangles.size() - 1].trianglePositions[2] = DirectX::XMFLOAT3(25.0f, 10.0f, 50.0f);
	triangles[triangles.size() - 1].normal = DirectX::XMFLOAT3(-1.0f, 0.0f, 0.0f);
	triangles[triangles.size() - 1].materialId = 2;//Blue
#pragma endregion

	model_ = new Model();
	model_->Load("../Models/sword.obj", 0, materials.size());
	triangles.insert(triangles.end(), model_->GetTriangles().begin(), model_->GetTriangles().end());
	
	materials.insert(materials.end(), model_->GetMaterials().begin(), model_->GetMaterials().end());
	AABB modelBox;
	for (unsigned int c = 10; c < triangles.size(); c++)
	{
		for (int k = 0; k < 3; k++)
		{
			modelBox.minCorner.x = min(modelBox.minCorner.x, triangles[c].trianglePositions[k].x);
			modelBox.minCorner.y = min(modelBox.minCorner.y, triangles[c].trianglePositions[k].y);
			modelBox.minCorner.z = min(modelBox.minCorner.z, triangles[c].trianglePositions[k].z);

			modelBox.maxCorner.x = max(modelBox.maxCorner.x, triangles[c].trianglePositions[k].x);
			modelBox.maxCorner.y = max(modelBox.maxCorner.y, triangles[c].trianglePositions[k].y);
			modelBox.maxCorner.z = max(modelBox.maxCorner.z, triangles[c].trianglePositions[k].z);
		}
	}

#pragma region Spheres
	std::vector<Sphere> spheres;
	spheres.push_back(Sphere(5.0f, 5.0f, 15.0f, 2.0f, 0));
	spheres.push_back(Sphere(0.0f, 5.0f, 15.0f, 2.0f, 1));
	spheres.push_back(Sphere(-5.0f, 5.0f, 15.0f, 2.0f, 0));

	spheres.push_back(Sphere(5.0f, 0.0f, 15.0f, 2.0f, 1));
	spheres.push_back(Sphere(0.0f, 0.0f, 15.0f, 2.0f, 2));
	spheres.push_back(Sphere(-5.0f, 0.0f, 15.0f, 2.0f, 0));

	spheres.push_back(Sphere(5.0f, -5.0f, 15.0f, 2.0f, 2));
	spheres.push_back(Sphere(0.0f, -5.0f, 15.0f, 2.0f, 0));
	spheres.push_back(Sphere(-5.0f, -5.0f, 15.0f, 2.0f, 1));

	if (!CreateSRVStructuredBuffer(spheres.size(), sizeof(Sphere), &spheres[0], &spheresSRV_))
		return false;
#pragma endregion




#pragma region VariableBuffer
	RandomData someVars;
	someVars.nrSpheres = spheres.size();
	someVars.nrTriangles = triangles.size();
	someVars.nrLights = lights.size();
	someVars.screenWidth = Graphics::instance().GetWindowWidth();
	someVars.screenHeight = Graphics::instance().GetWindowHeight();

	if (!CreateConstantBuffer(D3D11_USAGE_DEFAULT, 0, sizeof(RandomData), &someVars, variableBuffer_))
		return false;
#pragma endregion

	//Material Buffer
	if (!CreateConstantBuffer(D3D11_USAGE_IMMUTABLE, 0, sizeof(Model::Material)*materials.size(), &materials[0], materialBuffer_))
	{
		std::cout << "Failed to create material constant buffer" << std::endl;
		return false;
	}
	//AABB Buffer
	if (!CreateConstantBuffer(D3D11_USAGE_IMMUTABLE, 0, sizeof(AABB), &modelBox, aabbBuffer_))
	{
		std::cout << "Failed to create material constant buffer" << std::endl;
		return false;
	}
	Graphics::instance().GetDeviceContext()->CSSetConstantBuffers(4, 1, &aabbBuffer_);
	//Light Buffer
	if (!CreateConstantBuffer(D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, sizeof(PointLight)*lights.size(), &lights[0], lightBuffer_))
	{
		std::cout << "Failed to create lights constant buffer" << std::endl;
		return false;
	}
	//Triangle buffer
	if (!CreateSRVStructuredBuffer(triangles.size(), sizeof(Model::Triangle), &triangles[0], &trianglesSRV_))
		return false;

	//Create UAV for storing rays
	if (!CreateUAVStructuredBuffer(sizeof(Ray) * Graphics::instance().GetWindowWidth() * Graphics::instance().GetWindowHeight(), sizeof(Ray), nullptr, &raysUAV_))
		return false;
	if (!CreateUAVStructuredBuffer(sizeof(DirectX::XMFLOAT4) * Graphics::instance().GetWindowWidth() * Graphics::instance().GetWindowHeight(), sizeof(DirectX::XMFLOAT4), nullptr, &colorUAV_))
		return false;


	return true;
}


void Raytracer::Draw(double dt)
{
	(void)dt;


	//Set constant buffers
	ID3D11Buffer* cBuffers[] = { camera_->GetConstantBuffer(), variableBuffer_, materialBuffer_, lightBuffer_ };
	Graphics::instance().GetDeviceContext()->CSSetConstantBuffers(0, 4, cBuffers);

	//Set UAVs
	ID3D11UnorderedAccessView* uav[] = { raysUAV_, Graphics::instance().GetBackbufferUAV(), colorUAV_ };
	Graphics::instance().GetDeviceContext()->CSSetUnorderedAccessViews(0, 3, uav, NULL);

	//Set SRVs
	ID3D11ShaderResourceView* srv[] = { trianglesSRV_, spheresSRV_, model_->GetTextures()[0] };
	Graphics::instance().GetDeviceContext()->CSSetShaderResources(0, 3, srv);

	//Set Sampler State
	Graphics::instance().GetDeviceContext()->CSSetSamplers(0, 1, &samplerState_);

	//Calculate how many groups that should be dispatched
	UINT xDispatch = Graphics::instance().GetWindowWidth() / threadGroupSize_;
	UINT yDispatch = Graphics::instance().GetWindowHeight() / threadGroupSize_;

	timer->Start();
	//Primary Rays
	Graphics::instance().GetDeviceContext()->CSSetShader(primaryRaysCS_, NULL, 0);
	Graphics::instance().GetDeviceContext()->Dispatch(xDispatch, yDispatch, 1);
	for (unsigned int c = 0; c < nrBounces_; c++)
	{
		//Intersection stage
		Graphics::instance().GetDeviceContext()->CSSetShader(intersectionCS_, NULL, 0);
		Graphics::instance().GetDeviceContext()->Dispatch(xDispatch, yDispatch, 1);

		//Color Stage
		Graphics::instance().GetDeviceContext()->CSSetShader(colorCS_, NULL, 0);
		Graphics::instance().GetDeviceContext()->Dispatch(xDispatch, yDispatch, 1);
	}

	timer->Stop();

	count++;
	total += timer->GetTime();

	std::cout << "Average shader time in ms: " << total / count << std::endl;
	//std::cout << "Shader time in ms: " << timer->GetTime() << std::endl;
	Graphics::instance().Swap();
}

void Raytracer::Update(double dt)
{
	//Control Bounces during runtime
	if (Input::IsKeyPressed('I'))
	{
		nrBounces_++;
		std::cout << "Current number of bounces: " << nrBounces_ << std::endl;
	}
	if (Input::IsKeyPressed('O') && nrBounces_ > 1)
	{
		nrBounces_--;
		std::cout << "Current number of bounces: " << nrBounces_ << std::endl;
	}
	if (Input::IsKeyPressed('K')) {
		count = 0;
		total = 0;
	}

	//Updates camera and the cameras constant buffer
	camera_->Update(float(dt));

	//Move the lights and update the light buffer
	MoveLights();
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	Graphics::instance().GetDeviceContext()->Map(lightBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	memcpy(mappedResource.pData, &lights[0], sizeof(PointLight) * lights.size());
	Graphics::instance().GetDeviceContext()->Unmap(lightBuffer_, 0);

}








bool Raytracer::LoadComputeShader(std::string filepath, ID3D11ComputeShader** shader)
{
	ID3DBlob* source;
	if (D3DReadFileToBlob(std::wstring(filepath.begin(), filepath.end()).c_str(), &source))
	{
		MessageBox(NULL, "Failed to read compute shader file\n", "Error", MB_OK);
		return false;
	}

	if (FAILED(Graphics::instance().GetDevice()->CreateComputeShader(
		source->GetBufferPointer(),
		source->GetBufferSize(), classLink, shader)))
	{
		MessageBox(NULL, "Failed to create compute shader file\n", "Error", MB_OK);
		source->Release();
		return false;
	}

	//Shader reflection saker om vi vill ha ut vilka register som anv�nds
	source->Release();

	return true;
}

bool Raytracer::CreateConstantBuffer(D3D11_USAGE usage, UINT cpuAccess, unsigned int ByteWidth, void* initialData, ID3D11Buffer* &cBuffer)
{
	D3D11_BUFFER_DESC cBufferDesc;
	ZeroMemory(&cBufferDesc, sizeof(D3D11_BUFFER_DESC));
	cBufferDesc.Usage = usage;
	cBufferDesc.ByteWidth = ByteWidth;
	cBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cBufferDesc.CPUAccessFlags = cpuAccess;
	cBufferDesc.MiscFlags = 0;
	cBufferDesc.StructureByteStride = 0;


	HRESULT hr;
	if (initialData == nullptr)
		hr = Graphics::instance().GetDevice()->CreateBuffer(&cBufferDesc, nullptr, &cBuffer);
	else
	{
		D3D11_SUBRESOURCE_DATA resourceData;
		ZeroMemory(&resourceData, sizeof(resourceData));
		resourceData.pSysMem = initialData;
		hr = Graphics::instance().GetDevice()->CreateBuffer(&cBufferDesc, &resourceData, &cBuffer);
	}
	if (FAILED(hr))
	{
		_com_error err(hr);
		LPCTSTR errMsg = err.ErrorMessage();
		std::cout << "Raytracer::CreateConstantBuffer()\nCreateBuffer failed, ErrorMsg:\n" << std::string(errMsg) << std::endl;
		return false;
	}
	return true;

}

bool Raytracer::CreateUAVStructuredBuffer(unsigned int byteWidth, unsigned int stride, void* data, ID3D11UnorderedAccessView** uav)
{
	HRESULT hr;
	D3D11_BUFFER_DESC bDesc;
	bDesc.ByteWidth = byteWidth;
	bDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
	bDesc.CPUAccessFlags = 0;
	bDesc.StructureByteStride = stride;
	bDesc.Usage = D3D11_USAGE_DEFAULT;
	bDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;

	ID3D11Buffer* buffer;

	if (data == nullptr)
	{
		hr = Graphics::instance().GetDevice()->CreateBuffer(&bDesc, nullptr, &buffer);
		if (FAILED(hr))
		{
			MessageBox(NULL, "Raytracer::CreateUAVStructuredBuffer()\nFailed to create buffer\n", "Error", MB_OK);
			return false;
		}
	}
	else
	{
		D3D11_SUBRESOURCE_DATA dataResource;
		dataResource.pSysMem = &data;
		hr = Graphics::instance().GetDevice()->CreateBuffer(&bDesc, &dataResource, &buffer);
		if (FAILED(hr))
		{
			MessageBox(NULL, "Raytracer::CreateVertexBuffer()\nFailed to create buffer for vertices\n", "Error", MB_OK);
			return false;
		}
	}
	hr = Graphics::instance().GetDevice()->CreateUnorderedAccessView(buffer, nullptr, uav);

	if (FAILED(hr))
	{
		MessageBox(NULL, "Raytracer::CreateUAVStructuredBuffer()\nFailed to createUAV\n", "Error", MB_OK);
		buffer->Release();
		return false;
	}
	buffer->Release();
	return true;
}

bool Raytracer::CreateSRVStructuredBuffer(unsigned int numElements, unsigned int stride, void* data, ID3D11ShaderResourceView** srv)
{
	HRESULT hr;
	D3D11_BUFFER_DESC bDesc;
	ZeroMemory(&bDesc, sizeof(D3D11_BUFFER_DESC));
	bDesc.ByteWidth = numElements * stride;
	bDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	bDesc.CPUAccessFlags = 0;
	bDesc.StructureByteStride = stride;
	bDesc.Usage = D3D11_USAGE_DEFAULT;
	bDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;


	ID3D11Buffer* buffer;
	D3D11_SUBRESOURCE_DATA dataResource;
	ZeroMemory(&dataResource, sizeof(dataResource));
	dataResource.pSysMem = data;
	hr = Graphics::instance().GetDevice()->CreateBuffer(&bDesc, &dataResource, &buffer);
	if (FAILED(hr))
	{
		MessageBox(NULL, "Raytracer::CreateSRVStructuredBuffer() \nFailed to buffer\n", "Error", MB_OK);
		return false;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC sDesc;
	ZeroMemory(&sDesc, sizeof(sDesc));
	sDesc.Format = DXGI_FORMAT_UNKNOWN;
	sDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	sDesc.Buffer.ElementOffset = 0;
	sDesc.Buffer.ElementWidth = numElements;

	hr = Graphics::instance().GetDevice()->CreateShaderResourceView(buffer, &sDesc, srv);
	if (FAILED(hr))
	{
		MessageBox(NULL, "Raytracer::CreateUAVStructuredBuffer()\nFailed to createSRV\n", "Error", MB_OK);
		buffer->Release();
		return false;
	}
	buffer->Release();
	return true;
}

void Raytracer::MoveLights() {
	for (int i = 0; i < lights.size(); i++) {
		DirectX::XMFLOAT2 floathelvete;
		floathelvete.x = lights[i].Position.x;
		floathelvete.y = lights[i].Position.z;
		DirectX::XMVECTOR in = DirectX::XMLoadFloat2(&floathelvete);
		DirectX::XMVECTOR vektorhelvete = DirectX::XMVector2Orthogonal(in);
		DirectX::XMStoreFloat2(&floathelvete, vektorhelvete);
		lights[i].Position.x += floathelvete.x * 0.004f * (i+1);
		lights[i].Position.z += floathelvete.y * 0.004f * (i+1);
		float length = sqrtf(lights[i].Position.x * lights[i].Position.x + lights[i].Position.z * lights[i].Position.z);
		lights[i].Position.x = (lights[i].Position.x / length) * 9.5f;
		lights[i].Position.z = (lights[i].Position.z / length) * 9.5f;
	}
}