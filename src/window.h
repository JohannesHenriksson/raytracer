#ifndef WINDOW_H
#define WINDOW_H


#include <Windows.h>
#include <functional>
#include <iostream>

class Window {
public:
	Window(std::string name, long widht, long height);
	~Window();


	void Run();


	void SetDrawCallback(std::function<void(double milliseconds)> callback) { draw_callback_ = callback; };

	void SetUpdateCallback(std::function<void(double milliseconds)> callback) { update_callback_ = callback; };

	long GetWidth() { return windowWidth_; }
	long GetHeight() { return windowHeight_; }

	HWND GethWnd(){ return hWnd_; };


private:
	HINSTANCE hInstance_ = 0;
	HWND hWnd_ = 0;


	std::function<void(double milliseconds)> draw_callback_;
	std::function<void(double milliseconds)> update_callback_;

	static LRESULT CALLBACK Window::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


	long windowWidth_ = 1280;
	long windowHeight_ = 720;





};

#endif 