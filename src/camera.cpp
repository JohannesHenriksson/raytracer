#include "Camera.h"
#include "graphics.h"


Camera::Camera()
{
	right.x = -0.88f;
	right.y = 0;
	right.z = -0.47f;

	up.x = 0;
	up.y = 1;
	up.z = 0;

	direction_.x = -0.47f;
	direction_.y = 0;
	direction_.z = 0.88f;
	angle_ = 0;

	position_.x = 8.5f;
	position_.y = 0.0f;
	position_.z = -15.2f;

	D3D11_BUFFER_DESC cBufferDesc;
	ZeroMemory(&cBufferDesc, sizeof(D3D11_BUFFER_DESC));
	cBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	cBufferDesc.ByteWidth = sizeof(CameraBuffer);
	cBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cBufferDesc.MiscFlags = 0;
	cBufferDesc.StructureByteStride = 0;



	if (FAILED(Graphics::instance().GetDevice()->CreateBuffer(&cBufferDesc, nullptr, &constantBuffer_))) {
		MessageBox(NULL, "Camera::Camera() - CreateConstantBuffer\nError creating constant buffer.", "Error", MB_OK);
	}

}


Camera::~Camera() {
	if (constantBuffer_)
		constantBuffer_->Release();

}




DirectX::XMFLOAT3 Camera::GetPosition() {
	return position_;
}

void Camera::Update(float dt) {
	UpdateFreeFly(dt);

	//Update camera buffer
	Camera::CameraBuffer newCameraData;
	newCameraData.CamXValues.x = position_.x;
	newCameraData.CamXValues.y = direction_.x;
	newCameraData.CamXValues.z = up.x *-1;
	newCameraData.CamXValues.w = right.x;

	newCameraData.CamYValues.x = position_.y;
	newCameraData.CamYValues.y = direction_.y;
	newCameraData.CamYValues.z = up.y * -1;
	newCameraData.CamYValues.w = right.y;

	newCameraData.CamZValues.x = position_.z;
	newCameraData.CamZValues.y = direction_.z;
	newCameraData.CamZValues.z = up.z*-1;
	newCameraData.CamZValues.w = right.z;


	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	Graphics::instance().GetDeviceContext()->Map(constantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	memcpy(mappedResource.pData, &newCameraData, sizeof(newCameraData));
	Graphics::instance().GetDeviceContext()->Unmap(constantBuffer_, 0);
}

void Camera::SetConstantBuffer(unsigned int bindPoint) {
	Graphics::instance().GetDeviceContext()->CSSetConstantBuffers(bindPoint, 1, &constantBuffer_);
}

void Camera::UpdateFreeFly(float dt) {
	UpdatePosition(dt);
	if(Input::IsButtonDown(true))
		UpdateRotation();
}

void Camera::UpdateRotation()
{
	std::pair<int, int> mouseMovement = Input::GetMouseDelta();
	float x = DirectX::XMConvertToRadians(0.15f*mouseMovement.first);
	float y = DirectX::XMConvertToRadians(0.15f*mouseMovement.second);

	Pitch(-y);
	RotateY(-x);

}

void Camera::UpdatePosition(float dt)
{
	if (Input::IsKeyDown('W'))
		MoveForward(dt);
	if (Input::IsKeyDown('S'))
		MoveBackward(dt);
	if (Input::IsKeyDown('A'))
		MoveLeft(dt);
	if (Input::IsKeyDown('D'))
		MoveRight(dt);
	if (Input::IsKeyDown('Q'))
		MoveUp(dt);
	if (Input::IsKeyDown('E'))
		MoveDown(dt);


}

void Camera::MoveForward(float dt)
{
	DirectX::XMVECTOR pos = XMLoadFloat3(&position_);
	DirectX::XMVECTOR forwardVec = XMLoadFloat3(&direction_);
	if (Input::IsKeyDown(VK_SHIFT))
		forwardVec = DirectX::XMVectorMultiply(forwardVec, DirectX::XMVectorReplicate(dt));
	else
		forwardVec = DirectX::XMVectorMultiply(forwardVec, DirectX::XMVectorReplicate(camSpeed * dt));
	pos = DirectX::XMVectorAdd(pos, forwardVec);
	XMStoreFloat3(&position_, pos);
}

void Camera::MoveBackward(float dt)
{
	DirectX::XMVECTOR pos = XMLoadFloat3(&position_);
	DirectX::XMVECTOR forwardVec = XMLoadFloat3(&direction_);
	if (Input::IsKeyDown(VK_SHIFT))
		forwardVec = DirectX::XMVectorMultiply(forwardVec, DirectX::XMVectorReplicate(dt));
	else
		forwardVec = DirectX::XMVectorMultiply(forwardVec, DirectX::XMVectorReplicate(camSpeed * dt));
	pos = DirectX::XMVectorSubtract(pos, forwardVec);
	XMStoreFloat3(&position_, pos);
}

void Camera::MoveLeft(float dt)
{
	DirectX::XMVECTOR pos = XMLoadFloat3(&position_);
	DirectX::XMVECTOR rightVec = XMLoadFloat3(&right);
	if (Input::IsKeyDown(VK_SHIFT))
		rightVec = DirectX::XMVectorMultiply(rightVec, DirectX::XMVectorReplicate(dt));
	else
		rightVec = DirectX::XMVectorMultiply(rightVec, DirectX::XMVectorReplicate(camSpeed * dt));
	pos = DirectX::XMVectorSubtract(pos, rightVec);
	XMStoreFloat3(&position_, pos);
}

void Camera::MoveRight(float dt)
{
	DirectX::XMVECTOR pos = XMLoadFloat3(&position_);
	DirectX::XMVECTOR rightVec = XMLoadFloat3(&right);
	if (Input::IsKeyDown(VK_SHIFT))
		rightVec = DirectX::XMVectorMultiply(rightVec, DirectX::XMVectorReplicate(dt));
	else
		rightVec = DirectX::XMVectorMultiply(rightVec, DirectX::XMVectorReplicate(camSpeed * dt));
	pos = DirectX::XMVectorAdd(pos, rightVec);
	XMStoreFloat3(&position_, pos);
}
void Camera::MoveUp(float dt)
{
	DirectX::XMVECTOR pos = XMLoadFloat3(&position_);
	DirectX::XMVECTOR upVec = XMLoadFloat3(&up);
	if (Input::IsKeyDown(VK_SHIFT))
		upVec = DirectX::XMVectorMultiply(upVec, DirectX::XMVectorReplicate(dt));
	else
		upVec = DirectX::XMVectorMultiply(upVec, DirectX::XMVectorReplicate(camSpeed * dt));
	pos = DirectX::XMVectorAdd(pos, upVec);
	XMStoreFloat3(&position_, pos);
}
void Camera::MoveDown(float dt)
{
	DirectX::XMVECTOR pos = XMLoadFloat3(&position_);
	DirectX::XMVECTOR upVec = XMLoadFloat3(&up);
	if (Input::IsKeyDown(VK_SHIFT))
		upVec = DirectX::XMVectorMultiply(upVec, DirectX::XMVectorReplicate(dt));
	else
		upVec = DirectX::XMVectorMultiply(upVec, DirectX::XMVectorReplicate(camSpeed * dt));
	pos = DirectX::XMVectorSubtract(pos, upVec);
	XMStoreFloat3(&position_, pos);
}

void Camera::Pitch(float angle)
{
	DirectX::XMMATRIX rotation = DirectX::XMMatrixRotationAxis(XMLoadFloat3(&right), angle);

	DirectX::XMStoreFloat3(&up, DirectX::XMVector3TransformNormal(DirectX::XMLoadFloat3(&up), rotation));
	DirectX::XMStoreFloat3(&direction_, DirectX::XMVector3TransformNormal(DirectX::XMLoadFloat3(&direction_), rotation));
}

void Camera::RotateY(float angle)
{
	DirectX::XMMATRIX rotation = DirectX::XMMatrixRotationY(angle);

	DirectX::XMStoreFloat3(&right, DirectX::XMVector3TransformNormal(DirectX::XMLoadFloat3(&right), rotation));
	DirectX::XMStoreFloat3(&up, DirectX::XMVector3TransformNormal(DirectX::XMLoadFloat3(&up), rotation));
	DirectX::XMStoreFloat3(&direction_, DirectX::XMVector3TransformNormal(DirectX::XMLoadFloat3(&direction_), rotation));
}


