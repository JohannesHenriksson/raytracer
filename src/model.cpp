#include "model.h"

#include <iostream>
#include <fstream>
#include <comdef.h>
#include "ddstextureloader.h"
#include "graphics.h"

Model::Model()
{
}


Model::~Model()
{
	for (auto x : textures_)
		x->Release();
	for (auto x : texResources_)
		x->Release();
}


bool Model::Load(std::string filePath, int textureOffset, int materialOffset)
{
	std::ifstream fileStream(filePath);

	if (fileStream.fail())
	{
		std::cout << "Failed to load model file: " << filePath << std::endl;
		return false;
	}
	//Se om vi beh�ver ta ut path f�r att anv�nda vid mtllib
	std::string path;
	if (filePath.find_last_of("/") != std::string::npos)
		path = filePath.substr(0, filePath.find_last_of("/") + 1);
	else
		path = "";
	std::string currentWord;
	fileStream >> currentWord;
	std::vector<D3D11_SUBRESOURCE_DATA> textureDataArray;


	std::vector<DirectX::XMFLOAT3> positions;
	std::vector<DirectX::XMFLOAT3> normals;
	std::vector<DirectX::XMFLOAT2> texCoords;
	int currentMaterial = -1;

	while (!fileStream.eof())
	{
		if (currentWord == "#")
		{
			std::getline(fileStream, currentWord);
		}
		else if (currentWord == "v")
		{
			DirectX::XMFLOAT3 v;
			fileStream >> v.x;
			fileStream >> v.y;
			fileStream >> v.z;
			positions.push_back(v);
		}
		else if (currentWord == "vt")
		{
			//Skapa Texture Coordinate
			DirectX::XMFLOAT2 t;
			fileStream >> t.x;
			fileStream >> t.y;
			texCoords.push_back(t);
		}
		else if (currentWord == "vn")
		{
			//Ignore normals, we will calulate the normal for each triangle

			////Skapa Normal
			DirectX::XMFLOAT3 v;
			fileStream >> v.x;
			fileStream >> v.y;
			fileStream >> v.z;
			normals.push_back(v);

		}
		else if (currentWord == "f")
		{
			//Can faces come before vertex/normal/texcoord has been described in the obj file?
			Triangle t;
			if (currentMaterial == -1)
				t.materialId = -1;
			else
				t.materialId = currentMaterial + materialOffset;
			//Get the three vertex points that represent a face
			t.normal = DirectX::XMFLOAT3(0, 0, 0);
			for (unsigned int c = 0; c < 3; c++)
			{
				fileStream >> currentWord;
				unsigned int index = currentWord.find_first_of('/');
				t.trianglePositions[c] = positions[std::stoi(currentWord.substr(0, index)) - 1];
				t.normal.x += normals[std::stoi(currentWord.substr(0, index)) - 1].x;
				t.normal.y += normals[std::stoi(currentWord.substr(0, index)) - 1].y;
				t.normal.z += normals[std::stoi(currentWord.substr(0, index)) - 1].z;

				currentWord = currentWord.substr(index + 1, currentWord.size());
				index = currentWord.find_first_of('/');
				unsigned int i = std::stoi(currentWord.substr(0, index)) - 1;
				t.texCoords[c] = texCoords[i];

				index = std::stoi(currentWord.substr(0, index)) - 1;

			}
			t.normal.x /= 3.f;
			t.normal.y /= 3.f;
			t.normal.z /= 3.f;
			//TODO: implement CalculateNormal
			//t.normal = CalculateNormal(t);

			triangles_.push_back(t);
		}
		else if (currentWord == "usemtl")
		{
			fileStream >> currentWord;
			auto it = materialMap_.find(currentWord);
			if (it != materialMap_.end())
				currentMaterial = it->second;
			else
			{
				currentMaterial = 0;
			}
		}
		else if (currentWord == "mtllib")
		{
			int curMatIndex = 0;
			//Ladda in material library fil
			//Standardv�rden om det saknas i mtl filen.
			fileStream >> currentWord;
			currentWord = path + currentWord;
			std::ifstream materialStream(currentWord);
			if (materialStream.fail())
			{
				std::cout << "Failed to load material file: " << currentWord << std::endl;
			}
			std::string curMatToken;
			materialStream >> curMatToken;
			while (!materialStream.eof())
			{
				if (curMatToken == "Ka")
				{/*
				materialStream >> m->Ka.x;
				materialStream >> m->Ka.y;
				materialStream >> m->Ka.z;*/
					//std::cout << "Don't care about ambient color" << std::endl;
				}
				else if (curMatToken == "Kd")
				{
					materialStream >> materials_[curMatIndex].Kd.x;
					materialStream >> materials_[curMatIndex].Kd.y;
					materialStream >> materials_[curMatIndex].Kd.z;
					materials_[curMatIndex].reflectivity = min(min(materials_[curMatIndex].Kd.x, materials_[curMatIndex].Kd.y), materials_[curMatIndex].Kd.z);
				}
				else if (curMatToken == "Ks")
				{
					materialStream >> materials_[curMatIndex].SpecularColor.x;
					materialStream >> materials_[curMatIndex].SpecularColor.y;
					materialStream >> materials_[curMatIndex].SpecularColor.z;
				}
				else if (curMatToken == "d" || curMatToken == "Ts")
				{
					materialStream >> materials_[curMatIndex].transparency;
				}
				else if (curMatToken == "Ni")
				{
					materialStream >> materials_[curMatIndex].Ni;
				}
				else if (curMatToken == "Ns")
				{
					materialStream >> materials_[curMatIndex].Ns;
				}
				else if (curMatToken == "map_Kd")
				{
					//Ladda in textur
					materialStream >> curMatToken;
					curMatToken = path + curMatToken;
					std::wstring texName = std::wstring(curMatToken.begin(), curMatToken.end());
					ID3D11ShaderResourceView* newTex;
					ID3D11Resource* resource;
					//reinterpret_cast<ID3D11Resource**>(textures[0].ReleaseAndGetAddressOf()
					HRESULT hr = DirectX::CreateDDSTextureFromFile(Graphics::instance().GetDevice(), texName.c_str(), &resource, &newTex);
					if (FAILED(hr))
						std::cout << "Failed to load texture: " << curMatToken << std::endl;
					else
					{
						materials_[curMatIndex].textureIndex = textures_.size() + textureOffset;
						textures_.push_back(newTex);
						texResources_.push_back(resource);
					}
				}
				else if (curMatToken == "map_Ka")
				{
					//Ladda in textur
					std::cout << "Model::LoadModel material token: map_Ka not implemented" << std::endl;
					std::getline(materialStream, curMatToken);

				}
				else if (curMatToken == "newmtl")
				{
					std::getline(materialStream, curMatToken);
					materialMap_[curMatToken] = materials_.size();
					curMatIndex = materials_.size();
					materials_.push_back(Material());
				}
				else
				{
					std::cout << "Material line prefix not implemented: " << curMatToken << std::endl;
					std::getline(materialStream, curMatToken);
				}
				materialStream >> curMatToken;
			}
			materialStream.close();
		}
		else
		{
			//std::cout << "Model line prefix not implemented: " << currentWord << std::endl;
			std::getline(fileStream, currentWord);
		}


		fileStream >> currentWord;
	}
	fileStream.close();
	return true;
}





int Model::VertexExists(Vertex v)
{
	if (vertices_.size() == 0)
		return -1;
	for (unsigned int i = 0; i < vertices_.size(); i++)
	{
		if (CompareVec(vertices_[i].Position, v.Position) && CompareVec(vertices_[i].TexCoords, v.TexCoords) && CompareVec(vertices_[i].Normal, v.Normal))
			return i;
	}
	return -1;
}

//Slippa g�ra om till XMVECTOR, sigh
bool Model::CompareVec(DirectX::XMFLOAT3 v1, DirectX::XMFLOAT3 v2)
{
	return (v1.x == v2.x &&
		v1.y == v2.y &&
		v1.z == v2.z);
}
bool Model::CompareVec(DirectX::XMFLOAT2 v1, DirectX::XMFLOAT2 v2)
{
	return (v1.x == v2.x &&
		v1.y == v2.y);
}

void Model::CalculateNormal(Triangle t)
{
	DirectX::XMVECTOR v1 = DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&t.trianglePositions[1]), DirectX::XMLoadFloat3(&t.trianglePositions[0]));
	DirectX::XMVECTOR v2 = DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&t.trianglePositions[2]), DirectX::XMLoadFloat3(&t.trianglePositions[0]));

	DirectX::XMStoreFloat3(&t.normal, DirectX::XMVector3Cross(v1, v2));
}