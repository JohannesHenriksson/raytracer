#include "data.hlsl"





[numthreads(THREAD_GRP_SIZE, THREAD_GRP_SIZE, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	uint threadIndex = threadID.y * screenWidth + threadID.x;
	Ray r = rays[threadIndex];
	int oldTriangleId = r.triangleId;

	if (r.triangleId >= -1) {
		for (uint i = 0; i < 10; i++)
		{
			HitData t = Intersects(r, triangles[i]);
			if (t.t > 0.0f && t.t < r.tMax && r.triangleId != i)
			{
				r.tMax = t.t;
				r.triangleId = i;
				r.uv = t.uv;
			}
		}
		//Add check against object bounding box
		float3 dirFrac = 1.0f / r.Direction;
		if (IntesectsBox(r.Origin, objMinCorner, objMaxCorner, dirFrac))
		{
			for (uint i = 10; i < nrTriangles; i++)
			{
				HitData t = Intersects(r, triangles[i]);
				if (t.t > 0.0f && t.t < r.tMax && r.triangleId != i)
				{
					r.tMax = t.t;
					r.triangleId = i;
					r.uv = t.uv;
				}
			}
		}
		
			
		for (uint c = 0; c < nrSpheres; c++) {
			float t = Intersects(r, spheres[c]);
			if (t > 0.0f && t < r.tMax&& r.triangleId != c + nrTriangles) {
				r.tMax = t;
				r.triangleId = nrTriangles + c;
			}
		}
	}
	//If we found no intersection make sure we don't compute color or check for intersections on further bounces
	if (r.triangleId == -1 || oldTriangleId == r.triangleId)
		r.triangleId = -2;
	rays[threadIndex] = r;
}