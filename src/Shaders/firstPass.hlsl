
#define MAX_VERTICES 1024
#define MAX_INDICES 1024
#define NUM_TRIANGLES 2

struct Vertex {
	float3 vertPos;
	float3 vertNormal;
};

cbuffer CameraConstantBuffer : register(b0) {
	//(Position, Direction, Up, Right)
	float4 CamXValues;
	float4 CamYValues;
	float4 CamZValues;
}
cbuffer Vertices : register(b1) {
	Vertex vertices[MAX_VERTICES];
}
cbuffer Indices : register(b2) {
	uint indices[MAX_INDICES];
}

cbuffer LightBuffer : register(b3) {
	float4 light;
}


struct Sphere {
	float3 Center;
	float Radius;
};
struct Ray {
	float3 Origin;
	float3 Direction;
};

float Intersects(Ray r, float3 vertOne, float3 vertTwo, float3 vertThree);
float IntersectSphere(Ray r, Sphere s);

RWTexture2D<float4> output : register(u0);
RWTexture2D<float4> rays : register(u1);

[numthreads(32, 30, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	Sphere s;
	s.Center = light.xyz;
	s.Radius = light.w;
	
	
	Ray r;
	r.Origin = float3(CamXValues.x, CamYValues.x, CamZValues.x);
	float xPixel = (threadID.x / 1280.0f - 0.5f);
	float yPixel = (threadID.y / 720.0f - 0.5f)*(720.0/1280.0);
	r.Direction.x = xPixel * CamXValues.w + yPixel * CamXValues.z + CamXValues.y;
	r.Direction.y = xPixel * CamYValues.w + yPixel * CamYValues.z + CamYValues.y;
	r.Direction.z = xPixel * CamZValues.w + yPixel * CamZValues.z + CamZValues.y;

	r.Direction = normalize(r.Direction);

	float distance = -1;
	float3 color;
	for (uint i = 0; i < NUM_TRIANGLES; i++){
		uint index = i * 3;
		float t = Intersects(r, vertices[index].vertPos, vertices[index + 1].vertPos, vertices[index + 2].vertPos);
		if (t > 0) {
			distance = t;
			float3 lighttohit = light.xyz - (r.Origin + r.Direction * t);
			if (dot(vertices[index].vertNormal, lighttohit) > 0)
				color = vertices[index].vertNormal * (5.f / length(lighttohit));
			else
				color = float3(0, 0, 0);
		}
	}
	float t = IntersectSphere(r, s);
	if (t >= 0) {
		color = light.xyz;
		distance = t;
	}
	if (distance < 0)
		color = float3(1.0, 0, 0);
	output[threadID.xy] = float4(color, 1);

}

float Intersects(Ray r, float3 vertOne, float3 vertTwo, float3 vertThree) {
	
	float t, u, v;

	float3 edgeOne, edgeTwo;
	edgeOne = (vertTwo - vertOne);
	edgeTwo = (vertThree - vertOne);

	float3 pVector = cross(r.Direction, edgeTwo);

	float determinant = dot(edgeOne, pVector);

	if (determinant > -0.000001 && determinant < 0.000001)
		return -1.0f;

	float inverseDeterminant = 1 / determinant;

	float3 tVector = r.Origin - vertOne;
	u = dot(tVector, pVector) * inverseDeterminant;
	if (u < 0.0f || u > 1.0f)
		return -1.0f;

	float3 qVector = cross(tVector, edgeOne);
		v = dot(r.Direction, qVector) * inverseDeterminant;
	if (v < 0.0f || v+u > 1.0f)
		return -1.0f;

	t = dot(edgeTwo, qVector) * inverseDeterminant;
	if (t < 1)
		return -1.0f;

	//t = avst�nd till intersection fr�n origin
	//u och v �r de barycentriska koordinaterna

	return t;
}

float IntersectSphere(Ray r, Sphere s) {
	float b = dot(r.Direction, r.Origin - s.Center);
	float c = dot(r.Origin - s.Center, r.Origin - s.Center) - s.Radius * s.Radius;
	float f = b * b - c;
	if(f < 0)
		return -1.0f;
	if(-b - sqrt(f) > r.Origin.z)
		return -b - sqrt(f);
	else
		return -b + sqrt(f);
}