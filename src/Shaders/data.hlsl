#define RAY_RANGE = 100000.0f
#define MAX_LIGHTS 10
#define MAX_MATERIALS 5
#define THREAD_GRP_SIZE 16

//Structs
struct Ray {
	float3 Origin;
	float tMax;
	float3 Direction;
	int triangleId;
	float2 uv;
};
struct Sphere {
	float3 Center;
	float Radius;
	int materialId;
};
struct Light {
	float3 Position;
	float Range;
	float3 Color;
	float Attenuation;
};
struct Material {
	float3 Color;
	float transparency;
	float3 SpecularColor;
	float reflectivity;
	int textureIndex;
	float Ni;
	float Ns;
	float pad1;
	float4 pad2;
};
struct HitData
{
	float t;
	float2 uv;
};

struct Triangle {
	float3 trianglePositions[3];
	uint mortonCode;
	float3 normal;
	int materialId;
	float2 texCoords[3];
};

//ConstantBuffers
cbuffer CameraConstantBuffer : register(b0) 
{
	//(Position, Direction, Up, Right)
	float4 CamXValues;
	float4 CamYValues;
	float4 CamZValues;
}
cbuffer RandomData : register(b1) 
{
	uint nrTriangles;
	uint nrSpheres;
	uint nrLights;
	uint screenWidth;
	uint screenHeight;
	float3 padding;
}
cbuffer Materials : register(b2)
{
	Material materials[MAX_MATERIALS];
}
cbuffer Lights : register(b3)
{
	Light lights[MAX_LIGHTS];
}
cbuffer ObjectBoundingBox : register(b4)
{
	float3 objMinCorner;
	float padObjBox1;
	float3 objMaxCorner;
	float padObjBox2;
}
//UAVS
RWStructuredBuffer<Ray> rays : register(u0);
RWTexture2D<float4> output : register(u1);
RWStructuredBuffer<float4> colorBuffer : register(u2);

//SRVS
StructuredBuffer<Triangle> triangles : register(t0);
StructuredBuffer<Sphere> spheres : register(t1);

Texture2D objTexture : register(t2);

float Intersects(Ray r, float3 vertOne, float3 vertTwo, float3 vertThree) {

	float t, u, v;

	float3 edgeOne, edgeTwo;
	edgeOne = (vertTwo - vertOne);
	edgeTwo = (vertThree - vertOne);

	float3 pVector = cross(r.Direction, edgeTwo);

		float determinant = dot(edgeOne, pVector);

	if (determinant > -0.000001f && determinant < 0.000001f)
		return -1.0f;

	float inverseDeterminant = 1 / determinant;

	float3 tVector = r.Origin - vertOne;
		u = dot(tVector, pVector) * inverseDeterminant;
	if (u < 0.0f || u > 1.0f)
		return -1.0f;

	float3 qVector = cross(tVector, edgeOne);
		v = dot(r.Direction, qVector) * inverseDeterminant;
	if (v < 0.0f || v + u > 1.0f)
		return -1.0f;

	t = dot(edgeTwo, qVector) * inverseDeterminant;
	if (t < 0.0f)
		return -1.0f;

	//t = avst�nd till intersection fr�n origin
	//u och v �r de barycentriska koordinaterna

	return t;
}
HitData Intersects(Ray r, Triangle tri) {

	float t, u, v;
	HitData hData;
	hData.t = -1;
	hData.uv = float2(-1, -1);

	float3 edgeOne, edgeTwo;
	edgeOne = (tri.trianglePositions[1] - tri.trianglePositions[0]);
	edgeTwo = (tri.trianglePositions[2] - tri.trianglePositions[0]);

	float3 pVector = cross(r.Direction, edgeTwo);

	float determinant = dot(edgeOne, pVector);

	if (determinant > -0.000000000000000001 && determinant < 0.000000000000000001)
		return hData;

	float inverseDeterminant = 1 / determinant;

	float3 tVector = r.Origin - tri.trianglePositions[0];
	u = dot(tVector, pVector) * inverseDeterminant;
	if (u < 0.0f || u > 1.0f)
		return hData;

	float3 qVector = cross(tVector, edgeOne);
	v = dot(r.Direction, qVector) * inverseDeterminant;
	if (v < 0.0f || v + u > 1.0f)
		return hData;

	t = dot(edgeTwo, qVector) * inverseDeterminant;
	if (t < 0.0001f)
		return hData;

	//t = avst�nd till intersection fr�n origin
	//u och v �r de barycentriska koordinaterna
	hData.t = t;
	hData.uv = float2(u,v);
	return hData;
}
float IntersectsDist(Ray r, Triangle tri)
{
	float t, u, v;

	float3 edgeOne, edgeTwo;
	edgeOne = (tri.trianglePositions[1] - tri.trianglePositions[0]);
	edgeTwo = (tri.trianglePositions[2] - tri.trianglePositions[0]);

	float3 pVector = cross(r.Direction, edgeTwo);

	float determinant = dot(edgeOne, pVector);

	if (determinant > -0.000000000000000001 && determinant < 0.000000000000000001)
		return -1;

	float inverseDeterminant = 1 / determinant;

	float3 tVector = r.Origin - tri.trianglePositions[0];
	u = dot(tVector, pVector) * inverseDeterminant;
	if (u < 0.0f || u > 1.0f)
		return -1;

	float3 qVector = cross(tVector, edgeOne);
	v = dot(r.Direction, qVector) * inverseDeterminant;
	if (v < 0.0f || v + u > 1.0f)
		return -1;

	t = dot(edgeTwo, qVector) * inverseDeterminant;
	if (t < 0.0001f)
		return -1;

	return t;
}
float Intersects(Ray r, Sphere s) {
	//float b = dot(r.Direction, r.Origin - s.Center);
	//float c = dot(r.Origin - s.Center, r.Origin - s.Center) - s.Radius * s.Radius;
	//float f = b * b - c;
	//if (f < 0)
	//	return -1.0f;
	//if (-b - sqrt(f) > r.Origin.z)
	//	return -b - sqrt(f);
	//else
	//	return -b + sqrt(f);

	float a = dot(r.Direction, r.Direction);
	float b = dot(r.Direction, (2.0f*(r.Origin - s.Center)));
	float c = dot(s.Center, s.Center) + dot(r.Origin, r.Origin) - 2.0f * dot(r.Origin, s.Center) - s.Radius * s.Radius;
	float D = b*b + (-4.0f)*a*c;
	if (D < 0)
		return -1.0f;
	D = sqrt(D);
	float t = (-0.5f)*(b + D) / a;
	if (t > 0.0f) {
		t = sqrt(a)*t;
		return t;
	}
	else 
		return false;
}

//Intersection test for AABB using precomputed fraction of the direction
bool IntesectsBox(float3 rayOrigin, float3 minCorner, float3 maxCorner, float3 dirFrac)
{
	float t1 = (minCorner.x - rayOrigin.x)*dirFrac.x;
	float t2 = (maxCorner.x - rayOrigin.x)*dirFrac.x;
	float t3 = (minCorner.y - rayOrigin.y)*dirFrac.y;
	float t4 = (maxCorner.y - rayOrigin.y)*dirFrac.y;
	float t5 = (minCorner.z - rayOrigin.z)*dirFrac.z;
	float t6 = (maxCorner.z - rayOrigin.z)*dirFrac.z;

	float tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
	float tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));

	//Box behind origin
	if (tmax < 0)
		return false;

	//Missed box
	if (tmin > tmax)
		return false;

	//Box hit, at tmin
	return true;
}
//Ray AABB test
//bool Intesects(Ray r, AABB box)
//{
//	float3 dirFrac;
//	dirFrac.x = 1.0f / r.Direction.x;
//	dirFrac.y = 1.0f / r.Direction.y;
//	dirFrac.z = 1.0f / r.Direction.z;
//	
//	float t1 = (box.minCorner.x - r.Origin.x)*dirFrac.x;
//	float t2 = (box.maxCorner.x - r.Origin.x)*dirFrac.x;
//	float t3 = (box.minCorner.y - r.Origin.y)*dirFrac.y;
//	float t4 = (box.maxCorner.y - r.Origin.y)*dirFrac.y;
//	float t5 = (box.minCorner.z - r.Origin.z)*dirFrac.z;
//	float t6 = (box.maxCorner.z - r.Origin.z)*dirFrac.z;
//
//	float tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
//	float tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));
//
//	//Box behind origin
//	if (tmax < 0)
//		return false;
//
//	//Missed box
//	if (tmin > tmax)
//		return false;
//
//	//Box hit, at tmin
//	return true;
//}