#include "data.hlsl"

[numthreads(THREAD_GRP_SIZE, THREAD_GRP_SIZE, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	Ray r;
	r.Origin = float3(CamXValues.x, CamYValues.x, CamZValues.x);
	float xPixel = (threadID.x / (float)screenWidth - 0.5f);
	float yPixel = (threadID.y / (float)screenHeight - 0.5f)*((float)screenHeight / (float)screenWidth);
	r.Direction.x = xPixel * CamXValues.w + yPixel * CamXValues.z + CamXValues.y;
	r.Direction.y = xPixel * CamYValues.w + yPixel * CamYValues.z + CamYValues.y;
	r.Direction.z = xPixel * CamZValues.w + yPixel * CamZValues.z + CamZValues.y;

	r.Direction = normalize(r.Direction);

	r.triangleId = -1;
	r.tMax = 100000.0f;
	r.uv = float2(-1, -1);

	rays[threadID.y * screenWidth + threadID.x] = r;
	colorBuffer[threadID.y * screenWidth + threadID.x] = float4(0.0f, 0.0f, 0.0f, 1.0f);
}
