#include "data.hlsl"

bool LightIntersect(Ray r);

SamplerState MeshTextureSampler : register(s0);


[numthreads(THREAD_GRP_SIZE, THREAD_GRP_SIZE, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	uint threadIndex = threadID.y * screenWidth + threadID.x;

	Ray r = rays[threadIndex];
	//Om ray har tr�ffat n�got
	if (r.triangleId >= 0)
	{
		Ray lr;
		lr.Origin = r.Origin + r.Direction * r.tMax;
		lr.triangleId = r.triangleId;
		lr.uv = r.uv;

		r.tMax = 100000.0f;
		r.Origin = lr.Origin;
		float3 oldDir = rays[threadIndex].Direction;

		float3 normal;
		int materialId;
		if (lr.triangleId < nrTriangles)
		{
			normal = triangles[lr.triangleId].normal;
			materialId = triangles[lr.triangleId].materialId;
		}
		else
		{
			materialId = spheres[lr.triangleId - nrTriangles].materialId;
			normal = normalize(lr.Origin - spheres[lr.triangleId - nrTriangles].Center);
		}
        //Don't calculate colors for backfaces
        //if(dot(oldDir, normal) > 0)
        //{
        //    output[threadID.xy] = colorBuffer[threadID.y * screenWidth + threadID.x];
        //    return;
        //}
		r.Direction = oldDir - 2.f * dot(oldDir, normal) * normal;
		rays[threadIndex] = r;
		Material mat = materials[materialId];

		float3 color = mat.Color;
		int texId = mat.textureIndex;

		if (texId != -1)
		{
			float2 texCoord = lr.uv.x * triangles[lr.triangleId].texCoords[1] + lr.uv.y * triangles[lr.triangleId].texCoords[2] + (1.0f - (lr.uv.x + lr.uv.y)) * triangles[lr.triangleId].texCoords[0];
			color = objTexture.SampleLevel(MeshTextureSampler, texCoord, 0).xyz;
		}
		float4 colBufVal = colorBuffer[threadID.y * screenWidth + threadID.x];
		for (uint i = 0; i < nrLights; i++)
		{
			lr.Direction = lights[i].Position - lr.Origin;
			lr.tMax = length(lr.Direction);
			//Om ljuset �r in range och framf�r trianglen
			if (dot(normal, lr.Direction) > 0 && lr.tMax < lights[i].Range)
			{
				lr.Direction = normalize(lr.Direction);
				//Om inget �r i v�gen f�r ljuset
				if (LightIntersect(lr))
				{
					float brightness = saturate(dot(normal, lr.Direction) *  lights[i].Attenuation / lr.tMax);
					float SpecularFactor = dot(oldDir, reflect(lr.Direction, normal));
					float specLighting = pow(saturate(SpecularFactor), mat.Ns);

					if (materials[materialId].textureIndex != -1)
						colBufVal.xyz += (color * lights[i].Color * brightness + specLighting) * colBufVal.w;
					else
						colBufVal.xyz += (color * lights[i].Color * brightness + specLighting) * colBufVal.w * (1.0f - mat.reflectivity);
				}
			}
		}
		colorBuffer[threadID.y * screenWidth + threadID.x] = float4(colBufVal.xyz, colBufVal.w * mat.reflectivity);
	}
    output[threadID.xy] = colorBuffer[threadID.y * screenWidth + threadID.x];
}

bool LightIntersect(Ray r)
{
	for (uint i = 0; i < 10; i++)
	{
		float t = IntersectsDist(r, triangles[i]);
		if (t > 0.0f &&  t < r.tMax && i != r.triangleId)
			return false;
	}
    float3 dirFrac = 1.0f / r.Direction;
    if(IntesectsBox(r.Origin, objMinCorner, objMaxCorner, dirFrac))
    {
        for(uint i = 10; i < nrTriangles; i++)
        {
            HitData t = Intersects(r, triangles[i]);
            if(t.t > 0.0f && t.t < r.tMax && r.triangleId != i)
                return false;
        }
    }
	for (i = 0; i < nrSpheres; i++)
	{
		float t = Intersects(r, spheres[i]);
		if (t > 0.0f &&  t < r.tMax && (i + nrTriangles) != r.triangleId)
			return false;
	}
	return true;
}