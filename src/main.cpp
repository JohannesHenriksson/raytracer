
#include "window.h"
#include "graphics.h"
#include "input.h"
#include "raytracer.h"







int main()
{
	Window* window = new Window("Raytracer", 800, 800);
	Graphics::instance().Initialize(window);
	Raytracer raytracer = Raytracer();

	raytracer.Init();


	window->SetDrawCallback([&](double ms)->void{raytracer.Draw(ms);});
	window->SetUpdateCallback([&](double ms)->void{raytracer.Update(ms);});


	window->Run();

	delete window;

	return 0;
}


