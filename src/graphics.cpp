#include "graphics.h"
#include <dxgidebug.h>
#include "window.h"

#include <wrl/client.h>



Graphics::Graphics() {
	device_ = nullptr;
	deviceContext_ = nullptr;
	backbufferUAV_ = nullptr;
	swapChain_ = nullptr;
}

Graphics::~Graphics() {
	if (deviceContext_ != nullptr)
		deviceContext_->ClearState();
	if (backbufferUAV_ != nullptr)
		backbufferUAV_->Release();
	if (swapChain_ != nullptr)
		swapChain_->Release();
	if (deviceContext_ != nullptr)
		deviceContext_->Release();
	if (device_ != nullptr)
		device_->Release();
}

HRESULT Graphics::Initialize(Window* window) {
	parentWindow = window;
	HRESULT hr = E_FAIL;
	UINT flags = 0;
#if defined( DEBUG ) || defined( _DEBUG )
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif 
	D3D_DRIVER_TYPE driverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL featureLevels[] = { D3D_FEATURE_LEVEL_11_0 };
	D3D_FEATURE_LEVEL usedFeatureLevel = D3D_FEATURE_LEVEL_11_0;

	DXGI_SWAP_CHAIN_DESC scDesc;
	scDesc.BufferDesc.Width = parentWindow->GetWidth();
	scDesc.BufferDesc.Height = parentWindow->GetHeight();
	scDesc.BufferDesc.RefreshRate.Numerator = 60;
	scDesc.BufferDesc.RefreshRate.Denominator = 1;
	scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	scDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	scDesc.SampleDesc.Count = 1;
	scDesc.SampleDesc.Quality = 0;

	scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_UNORDERED_ACCESS;
	scDesc.BufferCount = 1;
	scDesc.OutputWindow = window->GethWnd();
	scDesc.Windowed = true;
	scDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	scDesc.Flags = 0;

	for (UINT driverType = 0; driverType < ARRAYSIZE(driverTypes) && FAILED(hr); driverType++) {
		hr = D3D11CreateDeviceAndSwapChain(
			nullptr,
			driverTypes[driverType],
			nullptr,
			flags,
			featureLevels,
			ARRAYSIZE(featureLevels),
			D3D11_SDK_VERSION,
			&scDesc,
			&swapChain_,
			&device_,
			&usedFeatureLevel,
			&deviceContext_);

		if (FAILED(hr))	{
			MessageBox(0, "D3D11CreateDeviceAndSwapChain Failed!", 0, 0);
			return hr;
		}
	}


	if (usedFeatureLevel != D3D_FEATURE_LEVEL_11_0)	{
		MessageBox(0, "Direct3D Feature level 11 is not supported", 0, 0);
		return hr;
	}

	ID3D11Texture2D* backBufferPointer;
	if (SUCCEEDED(hr = swapChain_->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPointer))) {
		hr = device_->CreateUnorderedAccessView(backBufferPointer, nullptr, &backbufferUAV_);
		if (FAILED(hr))	{
			MessageBox(0, "Failed to create backbuffer unordered access view", 0, 0);
			return hr;
		}
		backBufferPointer->Release();
	}

	//To hide specific warnings
	ID3D11Debug *d3dDebug = nullptr;
	if (SUCCEEDED(device_->QueryInterface(__uuidof(ID3D11Debug), (void**)&d3dDebug)))
	{
		ID3D11InfoQueue *d3dInfoQueue = nullptr;
		if (SUCCEEDED(d3dDebug->QueryInterface(__uuidof(ID3D11InfoQueue), (void**)&d3dInfoQueue)))
		{
#ifdef _DEBUG
			d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, true);
			d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, true);
#endif

			D3D11_MESSAGE_ID hide[] =
			{
				D3D11_MESSAGE_ID_DEVICE_DRAW_CONSTANT_BUFFER_TOO_SMALL
			};

			D3D11_INFO_QUEUE_FILTER filter;
			memset(&filter, 0, sizeof(filter));
			filter.DenyList.NumIDs = _countof(hide);
			filter.DenyList.pIDList = hide;
			d3dInfoQueue->AddStorageFilterEntries(&filter);
			d3dInfoQueue->Release();
		}
		d3dDebug->Release();
	}

	return hr;
}

void Graphics::Swap() {
	this->swapChain_->Present(0, 0);
}

ID3D11Device* Graphics::GetDevice() {
	return this->device_;
}
ID3D11DeviceContext* Graphics::GetDeviceContext() {
	return this->deviceContext_;
}
ID3D11UnorderedAccessView* Graphics::GetBackbufferUAV() {
	return this->backbufferUAV_;
}

long Graphics::GetWindowWidth()
{
	return parentWindow->GetWidth();
}

long Graphics::GetWindowHeight()
{
	return parentWindow->GetHeight();
}

void Graphics::ClearBackbufferUAV(float r, float g, float b, float a) {
	float color[4] = { r, g, b, a };
	this->deviceContext_->ClearUnorderedAccessViewFloat(this->backbufferUAV_, color);
}
