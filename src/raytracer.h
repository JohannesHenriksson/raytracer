#ifndef RAYTRACER_H_
#define RAYTRACER_H_
#include <string>
#include <d3d11.h>
#include <vector>
#include "camera.h"
#include "model.h"
#include "SLBVH.h"
#include "D3D11Timer.h"

#define SafeRelease(x) if(x != nullptr){x->Release(); x = nullptr;}

class Raytracer {
	 
public:
	struct PointLight {
		DirectX::XMFLOAT3 Position;
		float Range;
		DirectX::XMFLOAT3 Color;
		float Attenuation;
		PointLight(float px, float py, float pz, float r, float cx, float cy, float cz, float a) : Position(DirectX::XMFLOAT3(px, py, pz)), Range(r), Color(DirectX::XMFLOAT3(cx, cy, cz)), Attenuation(a) {};
	};
	struct Ray {
		DirectX::XMFLOAT3 Origin;
		float tMax;
		DirectX::XMFLOAT3 Direction;
		int triangleId;
		DirectX::XMFLOAT2 uv;
	};
	struct Vertex {
		DirectX::XMFLOAT4 Position;
		DirectX::XMFLOAT4 Normal;
		Vertex(float x, float y, float z, float nx, float ny, float nz) : Position(DirectX::XMFLOAT4(x, y, z, 1.0f)), Normal(DirectX::XMFLOAT4(nx, ny, nz, 1.0f)) {};
	};	


	struct Sphere {
		DirectX::XMFLOAT3 Position;
		float Radius;
		int materialId;

		Sphere(float xPos, float yPos, float zPos, float radius, int material) : 
			Position(DirectX::XMFLOAT3(xPos, yPos, zPos)), 
			Radius(radius), 
			materialId(material){};
	};
	struct AABB
	{
		DirectX::XMFLOAT3 minCorner;
		float pad1 = 0;
		DirectX::XMFLOAT3 maxCorner;
		float pad2 = 0;
	};
	struct RandomData {
		UINT nrTriangles;
		UINT nrSpheres;
		UINT nrLights;
		UINT screenWidth;
		UINT screenHeight;
		DirectX::XMFLOAT3 padding = DirectX::XMFLOAT3(0,0,0);
	};

	Raytracer();
	~Raytracer();

	bool Init();

	void Draw(double dt);
	void Update(double dt);

private:
	UINT threadGroupSize_ = 16;
	UINT nrBounces_ = 1;
	UINT nrLights = 10;

	int count = 0;
	double total = 0;

	//Objects
	Camera* camera_ = nullptr;


	//Shaders
	ID3D11ClassLinkage* classLink;

	ID3D11ComputeShader* primaryRaysCS_ = nullptr;
	ID3D11ComputeShader* intersectionCS_ = nullptr;
	ID3D11ComputeShader* colorCS_ = nullptr;

	ID3D11UnorderedAccessView* raysUAV_ = nullptr;
	ID3D11UnorderedAccessView* colorUAV_ = nullptr;


	std::vector<PointLight> lights;

	D3D11Timer* timer = nullptr;

	Model* model_ = nullptr;


	//SRVs
	ID3D11ShaderResourceView* trianglesSRV_ = nullptr;
	ID3D11ShaderResourceView* spheresSRV_ = nullptr;

	ID3D11SamplerState* samplerState_ = nullptr;

	//Constant Buffers
	ID3D11Buffer* variableBuffer_ = nullptr;
	ID3D11Buffer* lightBuffer_ = nullptr;
	ID3D11Buffer* materialBuffer_ = nullptr;
	ID3D11Buffer* aabbBuffer_ = nullptr;


	//Private methods
	bool LoadComputeShader(std::string filepath, ID3D11ComputeShader** shader);
	bool CreateConstantBuffer(D3D11_USAGE usage, UINT cpuAccess, unsigned int ByteWidth, void * initialData, ID3D11Buffer *& cBuffer);

	
	bool CreateUAVStructuredBuffer(unsigned int byteWidth, unsigned int stride, void* data, ID3D11UnorderedAccessView** uav);
	bool CreateSRVStructuredBuffer(unsigned int numElements, unsigned int stride, void* data, ID3D11ShaderResourceView** srv);

	void MoveLights();
};


#endif