#ifndef SLBVH_H_
#define SLBVH_H_

#include <d3d11.h>
#include <DirectXMath.h>
#include "model.h"

class SLBVH
{
public:
	struct Node
	{
		DirectX::XMFLOAT3 smallCorner = DirectX::XMFLOAT3(FLT_MAX, FLT_MAX, FLT_MAX);
		int primitiveOffset;
		DirectX::XMFLOAT3 bigCorner = DirectX::XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
		//2 least significant bits store the split axis, the third least significant bit store if the node is a leaf
		int primitiveCount;
	};

	SLBVH();
	~SLBVH();

	void Init(std::vector<Model::Triangle>& triangles);
	std::vector<Node>& GetTree();
private:
	void CreateSceneAABB();
	void CreateMortonCodes();
	void CreateBoundingBox(unsigned int nodeIndex, unsigned int primCount, unsigned int primOffset);
	void CreateNode(unsigned int nodeIndex, unsigned int primCount, unsigned int primOffset);

	unsigned int CountLeadingZeros(unsigned int i);
	unsigned int ExpandBits(unsigned int v);
	ID3D11UnorderedAccessView* treeUAV_ = nullptr;

	DirectX::XMFLOAT3 smallCorner = DirectX::XMFLOAT3(FLT_MAX, FLT_MAX, FLT_MAX);
	DirectX::XMFLOAT3 bigCorner = DirectX::XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	std::vector<Model::Triangle> triangles;
	std::vector<Node> nodes;


	const unsigned int MAX_DEPTH = 9;
	const unsigned int TREE_SIZE = (unsigned int)pow(2, MAX_DEPTH) - 1;

};









#endif