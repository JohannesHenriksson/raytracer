#include "SLBVH.h"
#include <iostream>
#include <bitset>


SLBVH::SLBVH()
{
}

SLBVH::~SLBVH()
{
}

void SLBVH::Init(std::vector<Model::Triangle>& triangleList)
{
	// + 1 since we don't use the first spot (index 0)
	nodes.resize(TREE_SIZE + 1);
	triangles = triangleList;
	//generate a bounding box that covers all scene geometry, needed to transform midpoints to the range [0, 1]
	CreateSceneAABB();
	//generate morton code for the triangles
	CreateMortonCodes();	

	//sort the triangles based on the morton code of each triangle, thus sorting them in the z-order curve
	std::qsort(&triangles[0], triangles.size(), sizeof(Model::Triangle), [](const void* a, const void* b)
	{
		//Comparison function
		Model::Triangle t1 = *static_cast<const Model::Triangle*>(a);
		Model::Triangle t2 = *static_cast<const Model::Triangle*>(b);

		if (t1.mortonCode < t2.mortonCode) return -1;
		if (t1.mortonCode > t2.mortonCode) return 1;
		return 0;
	});

	CreateNode(1, triangles.size(), 0);
	/*
	std::cout << "Efter sort:" << std::endl;
	for (int c = 0; c < triangles.size(); c++)
	{
		std::bitset<32> n = triangles[c].mortonCode;
		std::cout << n.to_string() << std::endl;
	}*/

}

std::vector<SLBVH::Node>& SLBVH::GetTree()
{
	return nodes;
}

void SLBVH::CreateSceneAABB()
{
	//G� igenom alla trianglar
	for (auto x : triangles)
	{
		//G� igenom triangles vertexpunkter
		for (int c = 0; c < 3; c++)
		{
			smallCorner.x = min(smallCorner.x, x.trianglePositions[c].x);
			smallCorner.y = min(smallCorner.z, x.trianglePositions[c].y);
			smallCorner.z = min(smallCorner.z, x.trianglePositions[c].z);

			bigCorner.x = max(bigCorner.x, x.trianglePositions[c].x);
			bigCorner.y = max(bigCorner.z, x.trianglePositions[c].y);
			bigCorner.z = max(bigCorner.z, x.trianglePositions[c].z);
		}
	}
}

void SLBVH::CreateMortonCodes()
{
	for (unsigned int c = 0; c < triangles.size(); c++)
	{
		//Calculate midpoint
		DirectX::XMFLOAT3 midpoint;
		midpoint.x = (triangles[c].trianglePositions[0].x + triangles[c].trianglePositions[1].x + triangles[c].trianglePositions[2].x) / 3;
		midpoint.y = (triangles[c].trianglePositions[0].y + triangles[c].trianglePositions[1].y + triangles[c].trianglePositions[2].y) / 3;
		midpoint.z = (triangles[c].trianglePositions[0].z + triangles[c].trianglePositions[1].z + triangles[c].trianglePositions[2].z) / 3;
		
		DirectX::XMVECTOR midVec, smallVec, bigVec;
		midVec = DirectX::XMLoadFloat3(&midpoint);
		smallVec = DirectX::XMLoadFloat3(&smallCorner);
		bigVec = DirectX::XMLoadFloat3(&bigCorner);
		//Transform midpoint to [0, 1]
		//midpoint = (midpoint - smallCorner) / (bigCorner - smallCorner);
		midVec = DirectX::XMVectorDivide(DirectX::XMVectorSubtract(midVec, smallVec), DirectX::XMVectorSubtract(bigVec, smallVec));
		DirectX::XMStoreFloat3(&midpoint, midVec);
		//Create Morton Code from midpoint
		float x = min(max(midpoint.x * 1024.0f, 0.0f), 1023.0f);
		float y = min(max(midpoint.y * 1024.0f, 0.0f), 1023.0f);
		float z = min(max(midpoint.z * 1024.0f, 0.0f), 1023.0f);
		unsigned int xx = ExpandBits((unsigned int)x);
		unsigned int yy = ExpandBits((unsigned int)y);
		unsigned int zz = ExpandBits((unsigned int)z);
		triangles[c].mortonCode = xx * 4 + yy * 2 + zz;
	}
}

void SLBVH::CreateBoundingBox(unsigned int nodeIndex, unsigned int primCount, unsigned int primOffset)
{
	for (unsigned int c = 0; c < primCount; c++)
	{
		for (int k = 0; k < 3; k++)
		{
			nodes[nodeIndex].smallCorner.x = min(nodes[nodeIndex].smallCorner.x, triangles[primOffset + c].trianglePositions[k].x);
			nodes[nodeIndex].smallCorner.y = min(nodes[nodeIndex].smallCorner.y, triangles[primOffset + c].trianglePositions[k].y);
			nodes[nodeIndex].smallCorner.z = min(nodes[nodeIndex].smallCorner.z, triangles[primOffset + c].trianglePositions[k].z);

			nodes[nodeIndex].bigCorner.x = max(nodes[nodeIndex].bigCorner.x, triangles[primOffset + c].trianglePositions[k].x);
			nodes[nodeIndex].bigCorner.y = max(nodes[nodeIndex].bigCorner.y, triangles[primOffset + c].trianglePositions[k].y);
			nodes[nodeIndex].bigCorner.z = max(nodes[nodeIndex].bigCorner.z, triangles[primOffset + c].trianglePositions[k].z);
		}
	}
}

void SLBVH::CreateNode(unsigned int nodeIndex, unsigned int primCount, unsigned int primOffset)
{
	if (primCount == 0)
		return;
	//Leaf if the morton codes is the same for the range will also be true if primcount is 1
	bool leaf = triangles[primOffset].mortonCode == triangles[primOffset + primCount - 1].mortonCode;
	//If it's an "internal" leaf, or we have reached the max depth so it has to be a leaf 
	if (leaf || ceil(log2(nodeIndex)) >= MAX_DEPTH)
	{
		nodes[nodeIndex] = Node();
		//Set leaf node and split axis here
		nodes[nodeIndex].primitiveCount = primCount << 3; //Set primitive count as the 29 most significant bits
		nodes[nodeIndex].primitiveCount |= 4; //Add the leaf flag in the third least significant bit
		nodes[nodeIndex].primitiveOffset = primOffset;
		//Create bounding box here
		CreateBoundingBox(nodeIndex, primCount, primOffset);

		//If it's an internal leaf invalidate the children
		if (2 * nodeIndex <= TREE_SIZE)
		{
			nodes[2 * nodeIndex].primitiveCount = INT_MIN;
			nodes[2 * nodeIndex].primitiveOffset = INT_MIN;
		}
		//If it's an internal leaf invalidate the children
		if (2 * nodeIndex + 1 <= TREE_SIZE)
		{
			nodes[2 * nodeIndex + 1].primitiveCount = INT_MIN;
			nodes[2 * nodeIndex + 1].primitiveOffset = INT_MIN;
		}
	}
	else
	{
		nodes[nodeIndex] = Node();
		//Set leaf node and split axis here
		//Shift 3 bits, it will fill with zeros, so the leaf flag is correct we just need to set the axis flag for the split aswell aswell
		
		nodes[nodeIndex].primitiveCount = primCount << 3;
		nodes[nodeIndex].primitiveOffset = primOffset;

		int suggestedSplit = primCount / 2;
		//Include all triangles with the same morton code in the upper part of the range
		for (unsigned int c = primOffset + suggestedSplit + 1; c < primOffset + (primCount - 1); c++)
			if (triangles[primOffset + suggestedSplit].mortonCode == triangles[c].mortonCode)
			{
				suggestedSplit++;
				c++;
			}
		//If we got the whole range, go from the middle towards the start of the range until we find a morton code that's not the same
		if (suggestedSplit == primCount)
		{
			suggestedSplit = primCount / 2;
			for (unsigned int c = primOffset + suggestedSplit -1; c < primOffset; c--)
				if (triangles[primOffset + suggestedSplit].mortonCode != triangles[c].mortonCode)
				{
					suggestedSplit = c;
					break;
				}
		}
		//Find what axis the split is on
		//X = 0, Y =  1, Z = 2
		int axis = (CountLeadingZeros(triangles[primOffset + suggestedSplit].mortonCode) - 2) % 3;
		//set the split axis
		nodes[nodeIndex].primitiveCount |= axis;


		unsigned int primCountLeft = suggestedSplit; 
		unsigned int primOffsetLeft = primOffset;

		unsigned int primCountRight = primCount - suggestedSplit;
		unsigned int primOffsetRight = primOffset + suggestedSplit;


		CreateNode(2 * nodeIndex, primCountLeft, primOffsetLeft);
		CreateNode(2 * nodeIndex + 1, primCountRight, primOffsetRight);


		//Expand Bounding Box
		nodes[nodeIndex].bigCorner.x = max(nodes[2 * nodeIndex].bigCorner.x, nodes[2 * nodeIndex + 1].bigCorner.x);
		nodes[nodeIndex].bigCorner.y = max(nodes[2 * nodeIndex].bigCorner.y, nodes[2 * nodeIndex + 1].bigCorner.y);
		nodes[nodeIndex].bigCorner.z = max(nodes[2 * nodeIndex].bigCorner.z, nodes[2 * nodeIndex + 1].bigCorner.z);

		nodes[nodeIndex].smallCorner.x = min(nodes[2 * nodeIndex].smallCorner.x, nodes[2 * nodeIndex + 1].smallCorner.x);
		nodes[nodeIndex].smallCorner.y = min(nodes[2 * nodeIndex].smallCorner.y, nodes[2 * nodeIndex + 1].smallCorner.y);
		nodes[nodeIndex].smallCorner.z = min(nodes[2 * nodeIndex].smallCorner.z, nodes[2 * nodeIndex + 1].smallCorner.z);		
	}
}

unsigned int SLBVH::CountLeadingZeros(unsigned int i)
{
	unsigned n = 0;
	const unsigned bits = sizeof(i) * 8;
	for (int c = 1; c < bits; c++)
	{
		if (i < 0) break;
		n++;
		i <<= 1;
	}
	return n;
}

unsigned int SLBVH::ExpandBits(unsigned int v)
{
	v = (v * 0x00010001u) & 0xFF0000FFu;
	v = (v * 0x00000101u) & 0x0F00F00Fu;
	v = (v * 0x00000011u) & 0xC30C30C3u;
	v = (v * 0x00000005u) & 0x49249249u;
	return v;
}