#include "input.h"


std::array<bool, 255> Input::previousKeyboardState_;
std::array<bool, 255> Input::keyboardState_;
std::pair<bool, bool> Input::buttonState_;
std::pair<bool, bool> Input::previousButtonState_;
std::pair<int, int> Input::mousePosition_;
std::pair<int, int> Input::previousMousePosition_;

Input::Input()
{
	keyboardState_.fill(false);
	previousKeyboardState_.fill(false);
}


Input::~Input()
{
}

void Input::SetButtonState(bool left, bool value) {
	if (left)
		buttonState_.first = value;
	else
		buttonState_.second = value;
}

void Input::SetMousePosition(int xPos, int yPos) {
	mousePosition_.first = xPos;
	mousePosition_.second = yPos;
}

void Input::SetKeyState(unsigned char key, bool value) {
		keyboardState_[key] = value;
}

bool Input::IsButtonDown(bool leftButton) {
	if (leftButton) return buttonState_.first;
	return buttonState_.second;
}

bool Input::IsButtonPressed(bool leftButton) {
	if (leftButton) return (buttonState_.first && !previousButtonState_.first);
	return (buttonState_.second && !previousButtonState_.second);
}

bool Input::IsKeyDown(char key) {
	return keyboardState_[key];
}
bool Input::IsKeyDown(int keyNumber)
{
	return keyboardState_[keyNumber];
}

bool Input::IsKeyPressed(char key)
{
	return (keyboardState_[key] && !previousKeyboardState_[key]);
}
bool Input::IsKeyPressed(int keyNumber)
{
	return (keyboardState_[keyNumber] && !previousKeyboardState_[keyNumber]);
}

bool Input::IsKeyReleased(char key)
{
	return (!keyboardState_[key] && previousKeyboardState_[key]);
}
bool Input::IsKeyReleased(int keyNumber)
{
	return (!keyboardState_[keyNumber] && previousKeyboardState_[keyNumber]);
}

void Input::PostUpdate() {
	previousKeyboardState_ = keyboardState_;
	previousButtonState_ = buttonState_;
	previousMousePosition_ = mousePosition_;
}

std::pair<int, int> Input::GetMouseDelta() {
	return std::pair<int, int>(mousePosition_.first-previousMousePosition_.first, mousePosition_.second-previousMousePosition_.second);
	
}
