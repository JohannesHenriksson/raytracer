#ifndef INPUT_H
#define INPUT_H

#include <Windows.h>
#include <array>

class Input {
public:

	static void SetButtonState(bool left, bool value);

	static void SetMousePosition(int xPos, int yPos);

	static void SetKeyState(unsigned char key, bool value);

	static bool IsButtonDown(bool leftButton);

	static bool IsButtonPressed(bool leftButton);

	static bool IsKeyDown(char key);

	static bool IsKeyDown(int keyNumber);

	static bool IsKeyPressed(char key);

	static bool IsKeyPressed(int keyNumber);

	static bool IsKeyReleased(char key);

	static bool IsKeyReleased(int keyNumber);

	static void PostUpdate();

	static std::pair<int, int> GetMouseDelta();

private:

	Input();
	~Input();

	
	static std::pair<bool, bool> buttonState_;
	static std::pair<bool, bool> previousButtonState_;
	static std::pair<int, int> mousePosition_;
	static std::pair<int, int> previousMousePosition_;
	static std::array<bool, 255> previousKeyboardState_;
	static std::array<bool, 255> keyboardState_;
};

#endif //INPUT_H