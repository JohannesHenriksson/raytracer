#include "input.h"
#include "window.h"



LRESULT Window::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)  {
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_MOUSEMOVE:
		POINT p;
		GetCursorPos(&p);
		Input::SetMousePosition(p.x, p.y);
		break;
	case WM_LBUTTONDOWN:
		Input::SetButtonState(true, true);
		break;
	case WM_LBUTTONUP:
		Input::SetButtonState(true, false);
		break;
	case WM_RBUTTONDOWN:
		Input::SetButtonState(false, true);
		break;
	case WM_RBUTTONUP:
		Input::SetButtonState(false, false);
		break;
	case WM_KEYUP:
		Input::SetKeyState((unsigned char)wParam, false);
		break;
	case WM_KEYDOWN:
		Input::SetKeyState((unsigned char)wParam, true);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}



Window::Window(std::string name, long width, long height) 
{
	windowWidth_ = width;
	windowHeight_ = height;
	hInstance_ = GetModuleHandle(NULL);

	WNDCLASSEX wndcex;
	wndcex.cbSize = sizeof(WNDCLASSEX);
	wndcex.style = CS_HREDRAW | CS_VREDRAW;
	wndcex.lpfnWndProc = WndProc;
	wndcex.cbClsExtra = 0;
	wndcex.cbWndExtra = 0;
	wndcex.hInstance = hInstance_;
	wndcex.hIcon = NULL;
	wndcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wndcex.lpszMenuName = NULL;
	wndcex.lpszClassName = "WindowClass";// wStringName.c_str();
	wndcex.hIconSm = 0;

	if (!RegisterClassEx(&wndcex)) {
		MessageBox(NULL, "Failed to register window class\nWindow::Window(std::string name)", "Error", MB_ICONERROR);
		return;
	}

	RECT windowRect = { 0, 0, windowWidth_, windowHeight_ };
	AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, false);

	hWnd_ = CreateWindow(
		"WindowClass",
		name.c_str(),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL,
		NULL,
		hInstance_,
		NULL);

	if (!hWnd_)	{
		MessageBox(0, "CreateWindow failed\nWindow::Window(std::string name)", "Error", MB_ICONERROR);
		return;
	}
	ShowWindow(hWnd_, 1);
}

Window::~Window() {
}

void Window::Run()
{

	//Temp timer
	__int64 cntsPerSec = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	double secsPerCnt = 1.0f / (double)cntsPerSec;

	__int64 prevTimeStamp = 0;



	// Main message loop
	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			__int64 currTimeStamp = 0;
			QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);
			//dt i ms
			double dt = (currTimeStamp - prevTimeStamp) * secsPerCnt;


			if (update_callback_)
				update_callback_(dt);
			if (draw_callback_)
				draw_callback_(dt);
			if (Input::IsKeyDown('X'))
				break;

			Input::PostUpdate();
			prevTimeStamp = currTimeStamp;
		}
	}
}